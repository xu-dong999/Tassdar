﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tassdar.Core.Mapper;
using Tassdar.Framework.Interface.Base;
using Tassdar.Framework.Logic.Base;
using Tassdar.Framework.Model.Base;
using Tassdar.Service.Entity;
using Tassdar.Service.Interface;
using Tassdar.Service.Request;

namespace Tassdar.Service.Logic
{
    public class UserLogic : LogicBase<UserEnitty>, IUser,IValidate<UserEnitty>
    {
        public ResultModel AddUser(UserRequest entity)
        {
            var data= Mapper.Map<UserRequest, UserEnitty>(entity);
            if (data.hasError)
            {
                return new ResultModel { Success=false,Message="对象装配失败"};
            }
            return AddEntity(data.target);
        }

        public IList<UserEnitty> GetUserList()
        {
            return GetEntities();
        }

        public (IList<UserEnitty>, int rows_count) GetUserList(int pageIndex, int pageSize)
        {
            int rows = 0;
            return (GetEntities(pageSize, pageIndex, out rows),rows);
        }

        public ResultModel Remove(int user_id)
        {
            return Remove(user_id);
        }

        public ResultModel UpdateUser(UserRequest entity)
        {
            var data = Mapper.Map<UserRequest, UserEnitty>(entity);
            if (data.hasError)
            {
                return new ResultModel { Success = false, Message = "对象装配失败" };
            }
            return EditEntity(data.target);
        }

        public (bool result, string message) Validate(UserEnitty entity)
        {
            if (Queryable.Any(x => x.UserName == entity.UserName))
            {
                return (false, "已经存在相同名称的用户");
            }
            return (false, "已经存在相同名称的用户");
        }
    }
}
