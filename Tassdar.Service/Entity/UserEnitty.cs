﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Tassdar.Framework.Model.Base;

namespace Tassdar.Service.Entity
{
    [Table("tassdar_user")]
   public class UserEnitty:EntityBase
    {
        public string UserName { get; set; }

        public string Address { get; set; }

        public int Age { get; set; }

        public int Sex { get; set; }
    }
}
