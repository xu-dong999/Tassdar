﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tassdar.Service.Request
{
    public class UserRequest
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Address { get; set; }

        public int Age { get; set; }

        public int Sex { get; set; }
    }
}
