﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tassdar.Framework.Model.Base
{
    /// <summary>
    /// 实体基类，提供一些公共参数，并且用于EF的实体扫描
    /// 使用者可以根据自己喜好进行扩展
    /// </summary>
   public class EntityBase: SlimEntityBase
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreatedTime { get; set; }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime? LastModifyTime { get; set; }

        /// <summary>
        /// 权重
        /// </summary>
        public int? SortOrder { get; set; }
    }
}
