﻿# Tassdar

#### 介绍
基于ASP.NET Core和EF Core的开发框架，适合于中小型项目的快速开发

#### 软件架构
ASP.NET Core WebAPI


#### 安装教程

1.安装.NET Core2.2以上SDK
2. 进入项目启动Tassdar.API
3. 访问/swagger

#### 使用说明

1. 项目基于MIT开源
2. 如果使用者较多会考虑进行下一步升级，比如提供异步支持
3. 本项目本着为.NET Core生态做贡献的原则，欢迎各位大佬指点，但是不喜勿喷。

#### 参与贡献

1. Fork 本仓库
2.Star

